#!bin/python
import bot 

def BnoOutput(bnoout):
	"""Turns output into a string"""
	retval = str()
	realstate = 'USA'
	if len(bnoout.keys()) > 5:
		realstate = bnoout['state'] 

	retval += realstate + ": "
	for inkey in bnoout.keys():
		if inkey != realstate:
			retval += f"{inkey}: {bnoout[inkey]} "

	return retval

def main():
    """Test bot stuff. Test bench."""
    # print(bot.ScrapeCovid())
    # print(bot.ScrapeBNO('Florida'))
    # print(bot.ScrapeBNO('New Jersey'))
    # print(bot.ScrapeBNO('Washington'))
    print(bot.ScrapeBNO('California'))
    # print(bot.ScrapeBNO('Texas'))
    print(bot.ScrapeBNO())
    # print(bot.ScrapeBNO('Bob'))
    print(bot.ScrapeBnoInt('Spain'))
    print(bot.ScrapeBnoInt())

if __name__ == '__main__':
    main()

