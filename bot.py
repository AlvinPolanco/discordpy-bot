import os
import discord
# from dotenv import load_dotenv
import urllib3
import json 
import re

# load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
strongVals = {
	'usconfirmed':0,
	'usrecovered':1,
	'usdead': 2,
	'caconfirmed':3,
	'carecovered':4,
	'cadead': 5,
}

bnoColumns = [
	'cases',
	'newcases',
	'deaths',
	'newdeaths',
	'deathrate',
	'sercrit',
	'recovered'
]

bnoCountry = [
	'cases',
	'deaths',
	'recovered',
	'unresolved'
]

bnoIntTotal = [
	'case',
	'deaths', 
	'recovered',
	'unresolved'
]

bnoIntColumns = [
	'cases',
	'newcases',
	'deaths',
	'newdeaths',
	'percdeath',
	'sercrit',
	'recovered',
	'blank0',
	'blank1',
	'cases',
	'deaths'
]

statejson = {}
with open("states.json",'r') as statefile:
	statejson = json.load(statefile)

def GetLatestUs():
	"""Gets the latest Covid-19 US summary"""
	link = "https://covidtracking.com/api/us"
	http = urllib3.PoolManager()		
	page = http.request('GET',link)
	pagejson = json.loads(page.data)
	retval = f"Positive: {pagejson[0]['positive']}  hospitalized: {pagejson[0]['hospitalized']} Deaths: {pagejson[0]['death']}"
	return retval 

def ScrapeCovid():
	"""Scrapes https://coronavirus.1point3acres.com/en for latest information."""

	link = "https://coronavirus.1point3acres.com/en" 
	http = urllib3.PoolManager()		
	page = http.request('GET',link)
	pageStr = str(page.data)

	indexMarker = "<strong class=\"jsx-1633900136\">"
	lastIndex = 0
	endChar = '<'
	valuesFound = []
	localindex = 0

	for _ in range(6): # Number of values to pluck
		localindex =  pageStr.index(indexMarker) + len(indexMarker)
		currentChar = pageStr[localindex]
		tmpStr = ''
		while currentChar != endChar:
			tmpStr += currentChar
			localindex += 1 
			currentChar = pageStr[localindex] 
		localindex += 1
		pageStr = pageStr[localindex:]
		valuesFound.append(tmpStr)

	# print("Values found")
	# for myval in valuesFound:
	#	 print(f"{myval}")
	retstr = f"US cases: {valuesFound[0]} US recov: {valuesFound[1]} US dead: {valuesFound[2]}\n"
	retstr += f"CA cases: {valuesFound[3]} CA recov: {valuesFound[4]} CA dead: {valuesFound[5]}"
	return retstr

def BnoOutput(bnoout):
	"""Turns output into a string"""
	retval = str()
	realstate = 'USA'
	if len(bnoout.keys()) > 5:
		realstate = bnoout['state'] 

	retval += realstate + " Covid-19 statistics:\n"
	for inkey in bnoout.keys():
		if inkey != 'state':
			retval += f"{inkey}: {bnoout[inkey]} -- ".title()

	return retval[:-4]

def BnoIntOutput(bnoout):
	"""Turns output into a string"""
	retval = str()
	realcountry = 'WorldWide'
	if len(bnoout.keys()) > 7:
		realcountry = bnoout['country'] 

	retval += realcountry + " Covid-19 statistics:\n "
	for inkey in bnoout.keys():
		if inkey != 'country':
			retval += f"{inkey}: {bnoout[inkey]} -- ".title()

	return retval[:-4]

def BnoState(datastr, state=None):
	pageStr = datastr
	giveUsStats = False
	giveTop5 = False
	t5retval = ''
	retval = ''
	localindex = 0
	cutstart = 0
	endChar = '<'
	markerText = "dir=\"ltr\">"
	markerLen = len(markerText)
	retvalState = {}
	if state == 'Top5':
		giveTop5 = True
	if state == None or state == '':
		giveUsStats = True
	else: 
		try:
			if not giveUsStats and not giveTop5:
				localindex = pageStr.index(state)
				pageStr=pageStr[localindex+16:]
				for column in range(6):
					tmpStr = ''
					localindexMarker = pageStr.index(markerText) + markerLen
					localindexChar = pageStr.index('\">') + 2
					localindex = localindexMarker if localindexMarker < localindexChar else localindexChar
					currentChar = pageStr[localindex]
					while currentChar != endChar:
						tmpStr += currentChar 
						localindex += 1
						currentChar = pageStr[localindex]
					if tmpStr == '':
						tmpStr = '0'
					localindex+=5
					pageStr = pageStr[localindex:]
					retvalState[bnoColumns[column]] = tmpStr 
					# print(f"{ bnoColumns[column] } : {tmpStr}")
				retvalState['state'] = state
			else:
				'''if Top5 selected'''
				for topFive in range(5):
					if (topFive % 2) == 0:
						markerText = "td class=\"s15\""
					else:
						markerText = "td class=\"s22\""
					markerIndex = pageStr.index(markerText)
					pageStr = pageStr[markerIndex + 25:]
					state = pageStr[:pageStr.index(endChar)]
					localindex = pageStr.index(state)
					pageStr = pageStr[localindex+16:]
					for column in range(6):
						tmpStr = ''
						localindexMarker = pageStr.index(markerText) + markerLen
						localindexChar = pageStr.index('\">') + 2
						localindex = localindexMarker if localindexMarker < localindexChar else localindexChar
						currentChar = pageStr[localindex]
						while currentChar != endChar:
							tmpStr += currentChar
							localindex += 1
							currentChar = pageStr[localindex]
						if tmpStr == '':
							tmpStr = '0'
						localindex += 5
						pageStr = pageStr[localindex:]
						retvalState[bnoColumns[column]] = tmpStr
						# print(f"{ bnoColumns[column] } : {tmpStr}")
					retvalState['state'] = state
					cutstart = len(state) + 22
					t5val = (BnoOutput(retvalState)) + "\n"
					t5retval += state + " " + t5val[cutstart:]
				return t5retval

		except ValueError as e:
			giveUsStats = True
			pass
		except Exception as e:
			raise e 

	if giveUsStats:
		retvalCountry = {}
		try: 
			# Initial index
			initialMark = 'RECOVERED'
			secondaryMark = '</th>'
			tripleMarker = '\">'
			endChar = '<'
			localindex = 0 

			# Get into position	
			localindex = pageStr.index(initialMark)
			pageStr = pageStr[localindex:]
			localindex = pageStr.index(secondaryMark)
			pageStr = pageStr[localindex:]
			for column in range(4):
				tmpStr = ''
				localindex = pageStr.index(tripleMarker) + len(tripleMarker)
				currentChar =  pageStr[localindex]
				while currentChar != endChar:
					tmpStr += currentChar
					localindex += 1
					currentChar = pageStr[localindex]
				if tmpStr == '':
					tmpStr = '0'
				localindex+=5
				pageStr = pageStr[localindex:]
				retvalCountry[bnoCountry[column]]=tmpStr
		except: 
			return str(e)
		finally:
			return retvalCountry
	return retvalState

def ScrapeBNO(state=None):
	"""Scrapes the BNO webpage for stats by state."""
	link = "https://docs.google.com/spreadsheets/d/e/2PACX-1vR30F8lYP3jG7YOq8es0PBpJIE5yvRVZffOyaqC0GgMBN6yt0Q-NI8pxS7hd1F9dYXnowSC6zpZmW9D/pubhtml/sheet?headers=false&gid=1902046093"
	http = urllib3.PoolManager()		
	page = http.request('GET',link)
	pageStr = str(page.data)

	lastIndex = 0
	endChar = '<'
	valuesFound = []
	localindex = 0
	if state != "Top5":
		# if state != None:
		# 	print(f"{state}")
		# else:
		# 	print('USA stats:')
		bnoreport = BnoState(pageStr, state)
		# print(bnoreport['cases'])
		return BnoOutput(bnoreport)
	else:
		# skip getting singular report from BnoOutputfor Top5, 
		# instead let BnoState compile top5
		return BnoState(pageStr, state)

def BnoInt(datastr,country):
	pageStr = datastr
	giveUsStats = False
	retval = ''
	localindex = 0
	endChar = '<'
	markerText = "dir=\"ltr\">"
	markerLen = len(markerText)
	retvalCountry = {}
	if country == None or country == '':
		giveUsStats = True
	else: 
		try:
			if not giveUsStats:
				localindex = pageStr.index('Links')
				pageStr=pageStr[localindex+16:]
				localindex = pageStr.index(country) # Repeat to not get stuck on a tab at the top
				pageStr=pageStr[localindex+5:]
				for column in range(7):
					tmpStr = ''
					localindexMarker = pageStr.index(markerText) + markerLen
					localindexChar = pageStr.index('\">') + 2
					localindex = localindexMarker if localindexMarker < localindexChar else localindexChar
					currentChar = pageStr[localindex]
					while currentChar != endChar:
						tmpStr += currentChar 
						localindex += 1
						currentChar = pageStr[localindex]
					if tmpStr == '':
						tmpStr = '0'
					localindex+=5
					pageStr = pageStr[localindex:]
					retvalCountry[ bnoIntColumns[column] ] = tmpStr 
					# print(f"{ bnoIntColumns[column] } : {tmpStr}")
				retvalCountry['country'] = country 

		except ValueError as e:
			giveUsStats = True
			pass
		except Exception as e:
			raise e 

	if giveUsStats:
		retvalCountry = {}
		try: 
			# Initial index
			initialMark = 'RECOVERED'
			secondaryMark = '</th>'
			tripleMarker = '\">'
			endChar = '<'
			localindex = 0 

			# Get into position	
			localindex = pageStr.index(initialMark)
			pageStr = pageStr[localindex:]
			localindex = pageStr.index(secondaryMark) 
			pageStr = pageStr[localindex:]
			for column in range(4):
				tmpStr = ''
				localindex = pageStr.index(tripleMarker) + len(tripleMarker)
				currentChar =  pageStr[localindex]
				while currentChar != endChar:
					tmpStr += currentChar
					localindex += 1
					currentChar = pageStr[localindex]
				if tmpStr == '':
					tmpStr = '0'
				localindex+=5
				pageStr = pageStr[localindex:]
				retvalCountry[bnoCountry[column]]=tmpStr
		except: 
			return str(e)
		finally:
			return retvalCountry
	return retvalCountry

def ScrapeBnoInt(country=None):
	"""Scrapes BNO international Google Sheet"""
	link = "https://docs.google.com/spreadsheets/d/e/2PACX-1vR30F8lYP3jG7YOq8es0PBpJIE5yvRVZffOyaqC0GgMBN6yt0Q-NI8pxS7hd1F9dYXnowSC6zpZmW9D/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false&amp;range=A1:I193"
	http = urllib3.PoolManager()		
	page = http.request('GET',link)
	pageStr = str(page.data)

	lastIndex = 0
	endChar = '<'
	valuesFound = []
	localindex = 0

	bnoReport = BnoInt(pageStr,country)
	return BnoIntOutput(bnoReport)
	# return bnoReport

def main():
	"""Executed function"""
	# Info/constnats 
	helpcommands = "Commands:\n"
	helpcommands += "covidus: covidus API response from https://covidtracking.com/api/us\n"
	# helpcommands += "covidam: scraped results from https://coronavirus.1point3acres.com/en"
	helpcommands += "bnous: argument of state name or blank for USA updated stats from BNO\n"
	helpcommands += "bnoint: argument of country or blank for WorldWide updated stats from BNO"

	# Bot activity
	client = discord.Client()

	bnopattern = re.compile("bnous")
	bnoIntPattern = re.compile("bnoint")

	@client.event
	async def on_ready():
		print(f'{client.user} has connected to Discord!')

	@client.event
	async def on_message(message):
		if message.content.lower() == 'helpmedago':
			await message.channel.send(helpcommands) 
		elif message.content.lower() == 'covidus':
			await message.channel.send(str(GetLatestUs()))
		# elif message.content.lower() == 'covidam':
		# 	await message.channel.send(ScrapeCovid())
		elif bnopattern.match(message.content.lower()):
			state = message.content[6:]
			'''check for abbreviation and convert'''
			try: 
				if len(state) <= 3: # just incase there is a 
					state = statejson['states'][(state[:2].upper())]
			except: 
				print(f"2 letter: {state} not found.")
				state = None
			if state == ' ' or state == '': 
				state = None
			print(f"state: {state}")
			await message.channel.send(ScrapeBNO(state))

		elif bnoIntPattern.match(message.content.lower()):
			country = message.content[7:]
			if country == ' ' or '' or None:
				country = None
			print(f"country: {country}")
			await message.channel.send(ScrapeBnoInt(country))
	client.run(TOKEN)

if __name__ == '__main__':
	main()
