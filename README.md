# DiscoVirus 

A discord bot to report some findings of the COVID-19 virus 

## Pre-reqs

It is best to use virtual env. Once started use `pip install -r requirements.txt`. 

## Use

1. Create a bot discord bot some how through the bot portal. 
1. Copy unique discord key to a file named `.env` with `export DISCORD_TOKEN=<key>` as it's contents. 
1. `source .env`
1. Start bot
1. Add bot with other guides online to channels you own. 

### Notes

Making UI tweaks for legibility

```
Pennsylvania: cases: 7,264 newcases: 1,262 deaths: 90 newdeaths: 11 deathrate: 1.24% sercrit: 1
```

becomes

```
Pennsylvania Covid-19 statistics:
Cases: 7,264 -- Newcases: 1,262 -- Deaths: 90 -- Newdeaths: 11 -- Deathrate: 1.24% -- Sercrit: 1
```

### Addition

Added Top5 lookup

```
bnous Top5
```

returns top 5 from BNO list

```
New York Cases: 113,704 -- Newcases: 0 -- Deaths: 3,914 -- Newdeaths: 0 -- Deathrate: 3.44% -- Sercrit: 4,126
New Jersey Cases: 34,124 -- Newcases: 0 -- Deaths: 846 -- Newdeaths: 0 -- Deathrate: 2.48% -- Sercrit: N/A
Michigan Cases: 14,225 -- Newcases: 0 -- Deaths: 540 -- Newdeaths: 0 -- Deathrate: 3.80% -- Sercrit: N/A
California Cases: 13,927 -- Newcases: 0 -- Deaths: 321 -- Newdeaths: 0 -- Deathrate: 2.30% -- Sercrit: 597
Louisiana Cases: 12,496 -- Newcases: 0 -- Deaths: 409 -- Newdeaths: 0 -- Deathrate: 3.27% -- Sercrit: 571
```